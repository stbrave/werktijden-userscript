# Werktijden userscript

Userscript om makkelijker je uren in te vullen

## Installatie instructies

1. Installeer Tampermonkey vanaf https://tampermonkey.net/ (of een andere userscript plugin)
2. Herstart je webbrowser
3. Klik [deze knop](https://bitbucket.org/stbrave/werktijden-userscript/raw/master/werktijden.user.js) of voeg [werktijden.user.js](https://bitbucket.org/stbrave/werktijden-userscript/raw/master/werktijden.user.js) handmatig toe.

## Features

Op dit moment 2 knoppen:

* Overleg (1 uur per dag)
* Werkzaamheden (7 uur per dag)

Dit ga ik nog verbeteren naar meer logica/prefixes.