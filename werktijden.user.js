// ==UserScript==
// @name         Werktijden automagisch uren invullen
// @namespace    stbrave/werktijden
// @homepageURL  https://bitbucket.org/stbrave/
// @updateURL    https://bitbucket.org/stbrave/werktijden-userscript/raw/master/werktijden.user.js
// @downloadURL  https://bitbucket.org/stbrave/werktijden-userscript/raw/master/werktijden.user.js
// @supportURL   https://bitbucket.org/stbrave/werktijden-userscript/issues/new
// @version      0.1.1
// @description  Maakt het makkelijker om je uren in te vullen
// @author       Stefan Thoolen
// @match        https://mijn.werktijden.nl/*
// @grant        GM_setValue
// @grant        GM_getValue
// ==/UserScript==

(function() {
    'use strict';

    // Add dialog
    let dialogHtml = '<form>';
    dialogHtml += '<div class="table-responsive"><table class="table table-condensed table-sticky"><tr><th colspan="2">Automagisch invullen waardes</th></tr>';
    dialogHtml += '<tr><td>Overleg start:</td><td><input type="text" id="overleg_start" class="userscript_storage form-control" value="09:00" autocomplete="off"></td></tr>';
    dialogHtml += '<tr><td>Overleg eind:</td><td><input type="text" id="overleg_eind" class="userscript_storage form-control" value="10:00"></td></tr>';
    dialogHtml += '<tr><td>Activiteiten start:</td><td><input type="text" id="activiteiten_start" class="userscript_storage form-control" value="10:00"></td></tr>';
    dialogHtml += '<tr><td>Activiteiten eind:</td><td><input type="text" id="activiteiten_eind" class="userscript_storage form-control" value="17:00"></td></tr>';
    dialogHtml += '</table></div>';
    dialogHtml += '</form>';
    let dialog = document.createElement('div');
    dialog.className = 'panel';
    dialog.innerHTML = dialogHtml;
    let panel = document.querySelector('.panel.panel-default');
    panel.parentNode.appendChild(dialog);

    // User storage working
    let storage = document.querySelectorAll('.userscript_storage');
    for (let i = 0; i < storage.length; ++i) {
        let storedValue = GM_getValue(storage[i].id);
        if (storedValue) {
            storage[i].value = storedValue;
        }
        storage[i].onblur = function() {
            GM_setValue(storage[i].id, storage[i].value);
        }
    }

    // Recall scrollTop
    if (GM_getValue('scrollTop')) {
        document.documentElement.scrollTop = GM_getValue('scrollTop');
        GM_setValue('scrollTop', null);
    }

    // Add "overleg" buttons
    let addBtns = document.getElementsByClassName('glyphicons-plus');
    for (let i = 0; i < addBtns.length; ++i) {
        let addBtnA = addBtns[i].parentNode;
        let btnContainer = addBtnA.parentNode;

        let scrumBtnA = document.createElement('a');
        scrumBtnA.setAttribute('href', "#");
        scrumBtnA.setAttribute('title', "Overleg invullen");
        scrumBtnA.innerHTML = '<span class="glyphicons glyphicons-skull"></span>';
        scrumBtnA.onclick = function(e) {
            GM_setValue('scrollTop', document.documentElement.scrollTop);
            e.preventDefault();
            addBtnA.click();
            window.setTimeout(function() {
                document.getElementById('ctl0_Content_StartTime').value = document.getElementById('overleg_start').value;
                document.getElementById('ctl0_Content_EndTime').value = document.getElementById('overleg_eind').value;
                document.getElementById('ctl0_Content_Positions').value = '8509'; // Intern Superbrave
                document.getElementById('ctl0_Content_HourCodes').value = '829'; // Events (Overleg) - Agile Events
                document.getElementById('ctl0_Content_ctl0').submit();
            }, 1000);
        }
        btnContainer.insertBefore(scrumBtnA, addBtnA);

        // Add "activiteiten" button
        let workBtnA = document.createElement('a');
        workBtnA.setAttribute('href', "#");
        workBtnA.setAttribute('title', "Activiteiten invullen");
        workBtnA.innerHTML = '<span class="glyphicons glyphicons-robot"></span>';
        workBtnA.onclick = function(e) {
            GM_setValue('scrollTop', document.documentElement.scrollTop);
            e.preventDefault();
            addBtnA.click();
            window.setTimeout(function() {
                document.getElementById('ctl0_Content_StartTime').value = document.getElementById('activiteiten_start').value;
                document.getElementById('ctl0_Content_EndTime').value = document.getElementById('activiteiten_eind').value;
                document.getElementById('ctl0_Content_Positions').value = '8507'; // e-Medvertise
                document.getElementById('ctl0_Content_HourCodes').value = '831'; // Werkzaamheden - Sprint Activiteiten
                document.getElementById('ctl0_Content_ctl0').submit();
            }, 1000);
        }
        btnContainer.insertBefore(workBtnA, addBtnA);
    }
})();